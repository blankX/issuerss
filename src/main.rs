mod github;
mod gitlab;

use std::env;
extern crate tokio;

fn main() {
    let mut args = env::args().skip(1);
    let rt = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap();
    match args.next().expect("Missing service").as_str() {
        "github" => rt.block_on(github::run(args)),
        "gitlab" => rt.block_on(gitlab::run(args)),
        _ => panic!("Unknown service"),
    };
}
